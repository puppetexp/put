class put::postgresqlserver {

  # postgres doesn't need a root password (also this does not work because:     $escaped = postgresql_escape($postgres_password) ) 

  #$root_pass = Deferred('put_getsecret', ['postgres/rootpassword'])
  class { 'postgresql::globals':
    encoding => 'UTF-8',
    locale   => 'en_US.UTF-8',
  }

  class { 'postgresql::server':
  ip_mask_deny_postgres_user => '0.0.0.0/0',
  ip_mask_allow_all_users    => '127.0.0.0/24',
  #ipv4acls                   => [''],
    #postgres_password          => $root_pass,
  }


}


