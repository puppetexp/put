class put::jumphost {


  hiera_hash('admins').each |String $username, Hash $user_hash| {

    user { $username:
      ensure         => present,
      purge_ssh_keys => true,
      managehome     => true,
      home           => "/home/${username}",
      shell          => '/bin/bash',
    }



    if $user_hash['sshkeys'] =~ Array {


      $user_hash[sshkeys].each |Hash $sshkey| {
        ssh_authorized_key { "jump_user: ${username}":
          ensure => present,
          user   => $username,
          type   => $sshkey['type'],
          key    => $sshkey['key']
        }
      }

    }  else {
      fail ('sshkeys we are expected an array, check hiera !')
    }

  }
}
