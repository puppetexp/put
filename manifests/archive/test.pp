# testtjes
class put::test {


  include put::tincserver

  put::tincnet{ 'testnetje':
    ip => '1.2.3.4/24'
  }

  #$secret = getsecret("jaa/lala1232213")

  #http://puppet-on-the-edge.blogspot.com/2018/10/the-topic-is-deferred.html
  #https://gist.github.com/binford2k/efd953569328baf3f64487b78fefbc62
  #https://www.hashicorp.com/resources/agent-side-lookups-with-hashicorp-vault-puppet-6
  #
  $secret2 = put_getsecret('jaa/lala')
  #$secret3 = put_lookup("jaa/lala")
  #$d3 = Deferred('put_lookup', ["secret/test"])
  $d2 = Deferred('put_getsecret', ['secret/test'])
  notify { "2: ${d2}": }
  $d4 = Deferred('put_getsecret', ['secret/testdeferrred'])
  #notify { "$4: {d4}": }

  $d5 = Deferred('put_getsecret', ['secret/testdeferrred5345'])
  notify { '5: ':
    message => $d5
  }

  $d6 = Deferred('put_getsecret', ['secret/testdeferrred234234324'])
  notify { '6: ':
    message => $d6
  }

  file{'/tmp/secretconfig.txt':
    content =>  Deferred('put_getsecret', ['secret/testdeferrred234234324'])

  }






  #$d = Deferred('vault_lookup::lookup', ["secret/test", 'https://vault.hostname:8200'])
  #$d = Deferred('vault_lookup::lookup', ["secret/test"])


}
