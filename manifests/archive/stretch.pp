# debian stretch
class put::stretch {

  include apt

  apt::source { 'debian_stretch':
    comment  => 'Debian stretch',
    location => 'http://httpredir.debian.org/debian/',
    release  => 'stretch',
    repos    => 'main contrib non-free',
    include  => {
      'src' => true,
      'deb' => true,
    },
  }
  -> apt::source { 'stretch_backport':
    ensure   => absent,
    location => 'http://deb.debian.org/debian/',
    release  => 'stretch-backports',
    repos    => 'main',
    pin      => '900',
    include  => {
      'src' => true,
      'deb' => true,
    },
  }
#  -> exec { 'apt-update release change':
#    command  => '/usr/bin/apt-get update --allow-releaseinfo-change',
#    path     => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
#    schedule => 'everyday',
#
#  }
  -> exec { 'apt-update':
    command  => '/usr/bin/apt-get update',
    path     => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    schedule => 'everyday',

  }
  -> exec { '--configure -a':
    command   => '/usr/bin/dpkg --configure -a',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
    schedule  => 'everyday',

  }

  -> exec { 'apt --fix-broken install':
    command   => '/usr/bin/apt --fix-broken install',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
    schedule  => 'everyday',

  }


  -> exec { 'apt-upgrade':
    command   => '/usr/bin/apt-get --quiet --yes --fix-broken upgrade',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
    schedule  => 'everyday',

  }

  -> exec { 'apt-get dist-upgrade':
    command   => '/usr/bin/apt-get --quiet --yes --fix-broken dist-upgrade',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
    schedule  => 'everyday',

  }

  -> exec { 'apt-upgrade2':
    command   => '/usr/bin/apt-get --quiet --yes --fix-broken upgrade',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
    schedule  => 'everyday',

  }


}
