# installs puppet6 agent
class put::puppet6 {
tag 'slow'

  #READ this: https://www.example42.com/2018/10/08/puppet6-ca-upgrading/




  # puppetlabs key
  apt::key { 'puppet gpg key':
    id     => '6F6B15509CF8E59E6E469F327F438280EF8D349F',
    server => 'pgp.mit.edu',
  }


  $gpgkey = @("EOF")
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBFe2Iz4BEADqbv/nWmR26bsivTDOLqrfBEvRu9kSfDMzYh9Bmik1A8Z036Eg
h5+TZD8Rrd5TErLQ6eZFmQXk9yKFoa9/C4aBjmsL/u0yeMmVb7/66i+x3eAYGLzV
FyunArjtefZyxq0B2mdRHE8kwl5XGl8015T5RGHCTEhpX14O9yigI7gtliRoZcl3
hfXtedcvweOf9VrV+t5LF4PrZejom8VcB5CE2pdQ+23KZD48+Cx/sHSLHDtahOTQ
5HgwOLK7rBll8djFgIqP/UvhOqnZGIsg4MzTvWd/vwanocfY8BPwwodpX6rPUrD2
aXPsaPeM3Q0juDnJT03c4i0jwCoYPg865sqBBrpOQyefxWD6UzGKYkZbaKeobrTB
xUKUlaz5agSK12j4N+cqVuZUBAWcokXLRrcftt55B8jz/Mwhx8kl6Qtrnzco9tBG
T5JN5vXMkETDjN/TqfB0D0OsLTYOp3jj4hpMpG377Q+6D71YuwfAsikfnpUtEBxe
NixXuKAIqrgG8trfODV+yYYWzfdM2vuuYiZW9pGAdm8ao+JalDZss3HL7oVYXSJp
MIjjhi78beuNflkdL76ACy81t2TvpxoPoUIG098kW3xd720oqQkyWJTgM+wV96bD
ycmRgNQpvqHYKWtZIyZCTzKzTTIdqg/sbE/D8cHGmoy0eHUDshcE0EtxsQARAQAB
tEhQdXBwZXQsIEluYy4gUmVsZWFzZSBLZXkgKFB1cHBldCwgSW5jLiBSZWxlYXNl
IEtleSkgPHJlbGVhc2VAcHVwcGV0LmNvbT6JAj4EEwECACgFAle2Iz4CGwMFCQlm
AYAGCwkIBwMCBhUIAgkKCwQWAgMBAh4BAheAAAoJEH9DgoDvjTSfIN0P/jcCRzK8
WIdhcNz5dkj7xRZb8Oft2yDfenQmzb1SwGGa96IwJFcjF4Nq7ymcDUqunS2DEDb2
gCucsqmW1ubkaggsYbc9voz/SQwhsQpBjfWbuyOX9DWmW6av/aB1F85wP79gyfqT
uidTGxQE6EhDbLe7tuvxOHfM1bKsUtI+0n9TALLLHfXUEdtaXCwMlJuO1IIn1PWa
H7HzyEjw6OW/cy73oM9nuErBIio1O60slPLOW2XNhdWZJCRWkcXyuumRjoepz7WN
1JgsLOTcB7rcQaBP3pDN0O/Om5dlDQ6oYitoJs/F0gfEgwK68Uy8k8sUR+FLLJqM
o0CwOg6CeWU4ShAEd1xZxVYW6VOOKlz9x9dvjIVDn2SlTBDmLS99ySlQS57rjGPf
GwlRUnuZP4OeSuoFNNJNb9PO6XFSP66eNHFbEpIoBU7phBzwWpTXNsW+kAcY8Rno
8GzKR/2FRsxe5Nhfh8xy88U7BA0tqxWdqpk/ym+wDcgHBfSRt0dPFnbaHAiMRlgX
J/NPHBQtkoEdQTKA+ICxcNTUMvsPDQgZcU1/ViLMN+6kZaGNDVcPeMgDvqxu0e/T
b3uYiId38HYbHmD6rDrOQL/2VPPXbdGbxDGQUgX1DfdOuFXw1hSTilwI1KdXxUXD
sCsZbchgliqGcI1l2En62+6pI2x5XQqqiJ7+
=HpaX
-----END PGP PUBLIC KEY BLOCK-----
EOF

  #copy puppet.conf
  $puppetconf = @("EOF")
[main]
    ssldir = /var/lib/puppet/ssl
    vardir = /var/lib/puppet
    certname = ${::fqdn}
    server = ${::puppet_master_server}

[agent]
daemonize = false
runinterval=2048h
EOF

  # for some reason, puppet fetching key from keyserver is not working....
  file { '/root/DEB-GPG-KEY-puppet':
    content => $gpgkey,
  }
  -> exec { 'manually import pup gpg key':
    command => 'apt-key add /root/DEB-GPG-KEY-puppet',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
  }
  -> apt::key { 'puppet6 gpg key':
    id     => '6F6B15509CF8E59E6E469F327F438280EF8D349F',
    server => 'pgp.mit.edu',
  }
  -> apt::source { 'puppet6':
    comment  => 'puppet6',
    location => 'http://apt.puppetlabs.com',
    repos    => 'puppet6',
    release  => $::lsbdistcodename,
    include  => {
#      'src' => true,
      'deb' => true,
    },
  }
  -> exec { 'apt-update pupp6':
    command => '/usr/bin/apt-get update',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    #schedule => 'everyday'
  }
  -> file { '/etc/puppetlabs':
    ensure => directory,
  }
  -> file { '/etc/puppetlabs/puppet':
    ensure => directory,
  }
  -> file { '/etc/puppetlabs/puppet/puppet.conf':
    content => $puppetconf,
  }
  -> file { '/etc/puppet/puppet.conf':
    content => $puppetconf,
  }
  -> package{ 'puppet6-release':
    ensure   => installed,
    provider => apt,
  }
  -> package{ 'puppet':
    ensure   => absent,
    provider => apt,
  }
  -> package{ 'puppet-agent':
    ensure   => installed,
    provider => apt,
  }


  service { 'puppet':
    ensure => stopped,
    enable => false,
  }


}

