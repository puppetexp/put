define put::mysqldb (
  String $dbname = $name,
  Optional[String] $sqlfile = undef,
  )
  {
    require put::mysqlserver
    $db_pass = Deferred('put_getsecret', ["mysqldb/${dbname}"])

    mysql::db { $dbname:
      user     => $dbname,
      password => $db_pass,
      host     => 'localhost',
      grant    => ['ALL'],
      #sql      => '/path/to/sqlfile.gz',
      #import_cat_cmd => 'zcat',
      #import_timeout => 900,
    }
  }
