#systemd user unit
define put::unit (
  String $unit_name = $title,
  String $user = 'nobody',
  String $command = '/usr/bin/sleep 60',
  $after = ['network.target', 'sshd.service'],
  $wants = [],
  #FIXME: env in userunit
  $environment = ['FIX=fix', 'ME=me'],
  #FIXME: cwd in useruni
  String $cwd = '/tmp/',
  Boolean $autorefresh = true,
  Boolean $autostart = true,
  Boolean $autorestart= false,
  $ensure = present,
  ){


  $envextra = ['DISPLAY=:0', 'PYTHONUNBUFFERED=1']


  $allenv = $environment + $envextra

  $unit_template = @(END)
<%- | String $name, String $cwd,  Array $after, String $command, Boolean $autostart, Boolean $autorestart, Array $wants | -%>
[Unit]
Description=<%= $name %>
StartLimitIntervalSec=0
#StartLimitInterval=200
#StartLimitBurst=5
<% unless $after =~ Array[Data,0,0] { -%>
<% $after.each -%>
<% |$a| { %>After=<%= $a %>

<% } -%>
<% } -%>

<% unless $wants =~ Array[Data,0,0] { -%>
<% $wants.each -%>
<% |$a| { %>Wants=<%= $a %>

<% } -%>
<% } -%>


[Service]
Type=simple
ExecStart=<%= $command %>
WorkingDirectory=<%= $cwd %>
User=<%= $user %>

<% unless $allenv =~ Array[Data,0,0] { -%>
<% $allenv.each -%>
<% |$e| { %>Environment=<%= $e %>

<% } -%>
<% } -%>


<% if $autorestart == true { -%>
Restart=always
#Restart=on-failure
RestartSec=30
<% } -%>


; Some security directives.
; Use private /tmp and /var/tmp folders inside a new file system namespace,
; which are discarded after the process stops.
PrivateTmp=true
; Mount /usr, /boot, and /etc as read-only for processes invoked by this service.
ProtectSystem=full
; Sets up a new /dev mount for the process and only adds API pseudo devices
; like /dev/null, /dev/zero or /dev/random but not physical devices. Disabled
; by default because it may not work on devices like the Raspberry Pi.
PrivateDevices=true
; Ensures that the service process and all its children can never gain new
; privileges through execve().
NoNewPrivileges=true
; This makes /home, /root, and /run/user inaccessible and empty for processes invoked
; by this unit. Make sure that you do not depend on data inside these folders.
ProtectHome=true
; Drops the sys admin capability from the daemon.
CapabilityBoundingSet=~CAP_SYS_ADMIN


[Install]
<% if $autostart == true { -%>
WantedBy=default.target
<% } -%>

END

  ### linger: sudo loginctl enable-linger "$USER"

  ### systemctl --user -t target

  ### systemctl daemon-reload

#  include put::userunitprep

# FIXME: linger doesn ot work on debian
#  exec { "enable linger ${unit_name}":
#    command => "loginctl enable-linger ${user}",
#    path    => '/usr/local/bin:/usr/bin:/bin',
#    #user    => $user,
#    #environment => ["HOME=/home/${user}", 'XDG_RUNTIME_DIR=/run/user/1000' ]
#
#  }

  file { "/etc/systemd/system/${name}.service":
    ensure  => file,
    content => inline_epp($unit_template, {
      'name'        => $unit_name,
      'command'     => $command,
      'after'       => $after,
      'cwd'         => $cwd,
      'autostart'   => $autostart,
      'autorestart' => $autorestart,
      'wants'       => $wants,
    }),
    notify  =>  [  Exec["daemon reload service ${unit_name}"],  Exec["enable service ${unit_name}"], Exec["start service ${unit_name}"],
                  Exec["restart service ${unit_name}"],
                ]

  }
  -> exec { "daemon reload service ${unit_name}":
      command     => 'systemctl daemon-reload',
      path        => '/usr/local/bin:/usr/bin:/bin',
      refreshonly => true,

  }
  -> exec { "enable service ${unit_name}":
      command     => "systemctl enable ${unit_name}",
      path        => '/usr/local/bin:/usr/bin:/bin',
      environment => ["HOME=/home/${user}", 'XDG_RUNTIME_DIR=/run/user/1000' ],
      refreshonly => true,

  }
  -> exec { "start service ${unit_name}":
      command     => "systemctl start ${unit_name}",
      path        => '/usr/local/bin:/usr/bin:/bin',
      refreshonly => true,

  }
  -> exec { "restart service ${unit_name}":
      command     => "systemctl restart ${unit_name}",
      path        => '/usr/local/bin:/usr/bin:/bin',
      refreshonly => true,

  }



}


