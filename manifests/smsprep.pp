# smsprep
class put::smsprep
(
  String $user = 'user',
  String $userdir = '/home/user',

)
{
  require sms::etcdefault 
  require put::userunitprep
  require put::debian
  require put::base
  require put::extrabase
  require put::scripts
  $pkgs = [
		'iptables',
  ]

  ensure_packages($pkgs, {ensure => 'installed'})


    $gitconf = @("EOF")
[user]
        email = user@${facts['networking']['hostname']}
        name = user
EOF

  file { "/home/${user}/.gitconfig":
    content => $gitconf,
    owner   => $user,
  }

  $rootgitconf = @("EOF")
[user]
        email = user@${facts['networking']['hostname']}
        name = user
EOF


  file { "/root/.gitconfig":
    content => $rootgitconf,
    owner   => 'root',
  }


  firewall { '991 avahi udp':
    dport  => '5353',
    proto  => 'udp',
    action => 'accept',
  }
  firewall { '992 avahi tcp':
    dport  => '5353',
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '991 avahi udp 60774' :
    dport  => '60774',
    proto  => 'udp',
    action => 'accept',
  }
  firewall { '992 avahi tcp 60774':
    dport  => '60774',
    proto  => 'tcp',
    action => 'accept',
  }

  file { "${userdir}/src/":
    ensure => directory,
    owner  => 'user',
  }


  file { "${userdir}/media":
    ensure => directory,
    owner  => 'user',

  }

}

