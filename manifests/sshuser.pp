# adds ssh keys from  hiera
class put::sshuser {

require put::ssh

  
  hiera_hash('admins').each |String $username, Hash $user_hash| {
    if $user_hash['sshkeys'] =~ Array {
      #notify {"sshkeys is array !": }

      $user_hash[sshkeys].each |Hash $sshkey| {
        #notify { "$username  + $sshkey['type'] ": } 

        ssh_authorized_key { "user: ${username}":
          ensure => present,
          user   => 'user',
          type   => $sshkey['type'],
          key    => $sshkey['key']
        }
      }

    }  else {
      fail ('sshkeys we are expected an array, check hiera !')
    }

  }

}
