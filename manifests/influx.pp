# installs influxdb with telegraf etc.
class put::influx {


  Package { 'toml-rb':
    ensure => installed,
    provider => 'gem'
  }
  # https://github.com/puppetlabs/influxdb
  #include influxdb

  class { 'influxdb':
    initial_org    => 'my_org',
    initial_bucket => 'my_bucket',
    admin_user => 'admin',
    admin_pass => Sensitive('puppetlabs')
  }

  influxdb_org { 'my_org':
    ensure => present,
  #  token  => $token,
    port   => 1234,
  }

  influxdb_bucket { 'my_bucket':
    ensure => present,
    org    => 'my_org',
    labels => ['my_label1', 'my_label2'],
  #  token  => $token,
    port   => 1234,
  }

  influxdb_bucket { 'telegraf':
    ensure => present,
    org    => 'my_org',
    labels => ['my_label1', 'my_label2'],
    #  token  => $token,
    port   => 1234,
  }


  # telegraf: https://github.com/voxpupuli/puppet-telegraf

  class { 'telegraf':
      hostname => $facts['hostname'],
      outputs  => {
          'influxdb' => [
              {
                  'urls'     => [ "http://localhost:8086"],
                  'database' => 'telegraf',
                  'username' => 'admin',
                  'password' => 'puppetlabs',
              }
          ]
      },
      inputs   => {
          'cpu' => [
              {
                  'percpu'   => true,
                  'totalcpu' => true,
              }
          ]
      }
  }

  # grafana

  class { 'grafana':
    cfg => {
      app_mode => 'production',
      server   => {
        http_port     => 8080,
      },
      users    => {
        allow_sign_up => true,
      },
    },
  }

  grafana_datasource { 'influxdb':
    grafana_url      => 'http://localhost:3000',
    grafana_user     => 'admin',
    grafana_password => '5ecretPassw0rd',
    grafana_api_path => '/grafana/api',
    type             => 'influxdb',
    organization     => 'NewOrg',
    url              => 'http://localhost:8086',
    user             => 'admin',
    password         => '1nFlux5ecret',
    database         => 'graphite',
    access_mode      => 'proxy',
    is_default       => true,
    #json_data        => template('path/to/additional/config.json'),
    #secure_json_data => template('path/to/additional/secure/config.json')
  }

# mqtt
include mosquitto


# homeassistant

class{'::homeassistant':
    location_name => 'Arc de Triomphe',
    latitude      => 48.8738,
    longitude     => 2.2950,
    elevation     => 300,
    unit_system   => 'metric',
    time_zone     => 'Europe/Amsterdam'
}

homeassistant::component{
  [
    'config',
    'http',
    'frontend',
    'updater',
    'discovery',
    'conversation',
    'history',
    'sun',
    'logbook',
    'esphome',
  ]:
}

homeassistant::component{'tts':
  config => {'platform' => 'google'}
}

homeassistant::component{'device_tracker':
  config => [
    {'platform' => 'netgear',
     'host'     => 'router.example.org',
     'username' => 'admin',
     'password' => 'secret',
    }
  ],
}

homeassistant::component{'myswitchs':
  component => 'switch',
  config    => [
                 {'platform' => 'google',
                 },
                 {'platform' => 'tplink',
                  'host'     => 'myplug.example.org',
                  'username' => 'foo',
                  'password' => 'bar',
                 },
               ],
  }

}
