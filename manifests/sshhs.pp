# ssh as hidden service
class put::sshhs {

   require tor

   tor::daemon::onion_service { 'onion-ssh':
     ports => [ '22' ],
     v3    => true,
   }


}
