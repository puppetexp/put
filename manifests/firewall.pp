class put::firewall {

  package { 'iptables-persistent': 
    ensure =>  installed 
  }
  
  class { ['put::firewall_pre', 'put::firewall_post']: }

  class { 'firewall': }

  Firewall {
    before  => Class['put::firewall_post'],
    require => Class['put::firewall_post'],
  }

}
