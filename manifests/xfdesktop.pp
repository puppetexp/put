# xfce desktop 
class put::xfdesktop
(
  String $xfsettings = '',
  String $user = 'user',
  Boolean $softwaregl = false,
  String $userdir = '/home/user',

)
{
  notice("xfdesktop included")

  class {'put::commondesktop':
    softwaregl => $softwaregl,
    session => 'xfce4',
  }

  $pkgs = [
    'desktop-base',
    'xfce4',
    'xserver-xorg-input-all',
    'chromium',
    #'webext-ublock-origin',
    'webext-ublock-origin-firefox',
    'webext-ublock-origin-chromium',
    #'xul-ext-ublock-origin',
    'firefox-esr',
    'vlc',
    'xserver-xorg',
    'xfce4',
    'terminator',
     'gimp',
    'inkscape',
    'terminator',
		'rclone',
	
  ]

  ensure_packages($pkgs, {ensure => 'installed'})


  #https://github.com/spanezz/lightdm-autologin-greeter

#  exec {'clear saved sessions':
#    command => "/usr/bin/rm /home/${user}/.cache/sessions/*",
#  }

#  exec {'chmod saved sessions':
#    command => "/usr/bin/chmod 500 /home/${user}/.cache/sessions",
#  }


#  exec {'delete xfce session':
#    command => "/usr/bin/rm -r /home/${user}/.config/xfce4-session",
#  }



  $kioskrc = @(EOF)
[xfce4-session]
SaveSession=NONE
CustomizeSplash=NONE
CustomizeChooser=NONE
CustomizeLogout=NONE
CustomizeCompatibility=%wheel
Shutdown=%wheel
CustomizeSecurity=%wheel

EOF


  file { '/etc/xdg/xfce4/kiosk':
    ensure => directory,
  }

  file { '/etc/xdg/xfce4/kiosk/kioskrc':
    content => $kioskrc,
  }

## FIXME: cp /etc/xdg/xfce4/panel/default.xml /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
  #this is supposed to stop xfce asking the question how you want your panel setup
  exec { 'default panel settings':
    command => 'cp /etc/xdg/xfce4/panel/default.xml /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml',
    path    => '/usr/local/bin:/usr/bin:/bin',
  }


  $xfconf = @(EOF)

thunar-volman /autobrowse/enabled        false
thunar-volman /automount-drives/enabled  true
thunar-volman /automount-media/enabled   false
thunar-volman /autoopen/enabled          false
xfce4-session /general/FailsafeSessionName          Failsafe
xfce4-session /general/SaveOnExit                   false
xfce4-session /general/SessionName                  Default
xfce4-session /shutdown/LockScreen                  false
xfce4-session /splash/Engine                        
xfce4-session /splash/engines/simple/BgColor        #214068
xfce4-session /splash/engines/simple/FgColor        #d2dae6
xfce4-session /splash/engines/simple/Font           Sans 8
xfce4-session /splash/engines/simple/Image          /usr/share/images/desktop-base/desktop-splash
xfce4-power-manager /xfce4-power-manager/brightness-switch                  0
xfce4-power-manager /xfce4-power-manager/brightness-switch-restore-on-exit  0
xfce4-power-manager /xfce4-power-manager/critical-power-action              2
xfce4-power-manager /xfce4-power-manager/hibernate-button-action            2
xfce4-power-manager /xfce4-power-manager/lid-action-on-ac                   0
xfce4-power-manager /xfce4-power-manager/lid-action-on-battery              0
xfce4-power-manager /xfce4-power-manager/lock-screen-suspend-hibernate      false
xfce4-power-manager /xfce4-power-manager/logind-handle-lid-switch           false
xfce4-power-manager /xfce4-power-manager/power-button-action                3
xfce4-power-manager /xfce4-power-manager/presentation-mode                  true
xfce4-power-manager /xfce4-power-manager/show-tray-icon                     0
xfce4-power-manager /xfce4-power-manager/sleep-button-action                1
xsettings /Gtk/ButtonImages               true
xsettings /Gtk/CanChangeAccels            false
xsettings /Gtk/ColorPalette               black:white:gray50:red:purple:blue:light blue:green:yellow:orange:lavender:brown:goldenrod4:dodger blue:pink:light green:gray10:gray30:gray75:gray90
xsettings /Gtk/CursorThemeName            
xsettings /Gtk/CursorThemeSize            0
xsettings /Gtk/DecorationLayout           menu:minimize,maximize,close
xsettings /Gtk/FontName                   Sans 10
xsettings /Gtk/IconSizes                  
xsettings /Gtk/KeyThemeName               
xsettings /Gtk/MenuBarAccel               F10
xsettings /Gtk/MenuImages                 true
xsettings /Gtk/MonospaceFontName          Monospace 10
xsettings /Gtk/ToolbarIconSize            3
xsettings /Gtk/ToolbarStyle               icons
xsettings /Net/CursorBlink                true
xsettings /Net/CursorBlinkTime            1200
xsettings /Net/DndDragThreshold           8
xsettings /Net/DoubleClickDistance        5
xsettings /Net/DoubleClickTime            400
xsettings /Net/EnableEventSounds          false
xsettings /Net/EnableInputFeedbackSounds  false
xsettings /Net/IconThemeName              Rodent
xsettings /Net/SoundThemeName             default
xsettings /Net/ThemeName                  Xfce-dusk
xsettings /Xft/Antialias                  -1
xsettings /Xft/Hinting                    -1
xsettings /Xft/HintStyle                  hintnone
xsettings /Xft/RGBA                       none
xfce4-desktop /backdrop/screen0/monitor0/brightness              0
xfce4-desktop /backdrop/screen0/monitor0/color-style             1
xfce4-desktop /backdrop/screen0/monitor0/image-path              /usr/share/images/desktop-base/default
xfce4-desktop /backdrop/screen0/monitor0/image-show              true
xfce4-desktop /backdrop/screen0/monitor0/last-image              /usr/share/images/desktop-base/default
xfce4-desktop /backdrop/screen0/monitor0/last-single-image       /usr/share/images/desktop-base/default
xfce4-desktop /backdrop/screen0/monitor0/workspace0/color-style  0
xfce4-desktop /backdrop/screen0/monitor0/workspace0/image-style  5
xfce4-desktop /backdrop/screen0/monitor0/workspace1/color-style  0
xfce4-desktop /backdrop/screen0/monitor0/workspace1/image-style  5
xfce4-desktop /backdrop/screen0/monitor0/workspace2/color-style  0
xfce4-desktop /backdrop/screen0/monitor0/workspace2/image-style  5
xfce4-desktop /backdrop/screen0/monitor0/workspace3/color-style  0
xfce4-desktop /backdrop/screen0/monitor0/workspace3/image-style  5
xfce4-desktop /backdrop/screen0/monitor1/brightness              0
xfce4-desktop /backdrop/screen0/monitor1/color-style             1
xfce4-desktop /backdrop/screen0/monitor1/image-path              /usr/share/images/desktop-base/default
xfce4-desktop /backdrop/screen0/monitor1/image-show              true
xfce4-desktop /backdrop/screen0/monitor1/last-image              /usr/share/images/desktop-base/default
xfce4-desktop /backdrop/screen0/monitor1/last-single-image       /usr/share/images/desktop-base/default
xfce4-panel /configver                           2
xfce4-panel /panels/panel-1/length               100
xfce4-panel /panels/panel-1/position             p=6;x=0;y=0
xfce4-panel /panels/panel-1/position-locked      true
xfce4-panel /panels/panel-1/size                 30
xfce4-panel /plugins/plugin-1                    applicationsmenu
xfce4-panel /plugins/plugin-15                   separator
xfce4-panel /plugins/plugin-15/expand            true
xfce4-panel /plugins/plugin-15/style             0
xfce4-panel /plugins/plugin-16                   power-manager-plugin
xfce4-panel /plugins/plugin-1/show-button-title  false
xfce4-panel /plugins/plugin-2                    genmon
xfce4-panel /plugins/plugin-3                    tasklist
xfce4-panel /plugins/plugin-4                    pager
xfce4-panel /plugins/plugin-4/miniature-view     true
xfce4-panel /plugins/plugin-4/rows               2
xfce4-panel /plugins/plugin-5                    clock
xfce4-panel /plugins/plugin-6                    systray
xfce4-panel /plugins/plugin-7                    screenshooter
keyboards /Default/Numlock  true
EOF


  file{ '/etc/xdg/autostart/light-locker.desktop':
    ensure => absent,
  }


  #set "/files/etc/lightdm/lightdm.conf/Seat:*/autologin-user-timeout" "5"
#  augeas { 'autologin-user':
##    #context => "/files/etc/hosts",
#    changes => [
#      'set /files/etc/lightdm/lightdm.conf/Seat:*/autologin-user-timeout 5',
#      "set /files/etc/lightdm/lightdm.conf/Seat:*/autologin-user ${user}"
#    ],
#  }

#  $timestamp = generate('/bin/date', '+%Y-%m-%dT%H:%M:%S')


#  file { "/home/${user}/.puppet":
#    ensure => directory,
#    owner  => $user,
#  }
#  -> file { "/home/${user}/.puppet/xfsettings":
#    content => $xfconf,
#    owner   => $user,
#  }
#  -> file { "/home/${user}/.puppet/xfsettings.wtf":
#    content => $xfconf,
#    owner   => $user,
#  }
#  -> exec { 'backup xfsettings':
#      command     => "/usr/local/bin/xfsave.sh /home/${user}/.puppet/xfsettings.${timestamp}",
#      path        => '/usr/local/bin:/usr/bin:/bin',
#      user        => $user,
#      environment => ["HOME=/home/${user}", 'XDG_RUNTIME_DIR=/run/user/1000' ]
#  }
# FIXME: thist does not seem to work
#  -> exec { 'restore xfsettings':
#     command     => "/usr/local/bin/xfrestore.sh /home/${user}/.puppet/xfsettings",
#      path        => '/usr/local/bin:/usr/bin:/bin',
#      user        => $user,
#      environment => ["HOME=/home/${user}", 'XDG_RUNTIME_DIR=/run/user/1000' ]
#  }

  # disable screen sleeping
  #
  file { "/home/user/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-power-manager.xml":
    content => epp("${module_name}/xfce4-power-manager.xml.epp"),
  }



}

