class put::firewall_pre {
  Firewall {
    require => undef,
  }

  # Default firewall rules
  firewall { '000 accept all icmp':
    proto  => 'icmp',
    action => 'accept',
  }
  -> firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }
  -> firewall { '002 reject local traffic not on loopback interface':
    iniface     => '! lo',
    proto       => 'all',
    destination => '127.0.0.1/8',
    action      => 'reject',
  }
  -> firewall { '003 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }
  -> firewall { '004 Allow inbound SSH':
    dport  => 22,
    proto  => tcp,
    action => accept,
  }

  # Default v6 firewall rules
  firewall { '000 v6 accept all icmp':
    proto    => 'icmp',
    action   => 'accept',
    provider => 'ip6tables',
  }
  -> firewall { '001 v6 accept all to lo interface':
    proto    => 'all',
    iniface  => 'lo',
    action   => 'accept',
    provider => 'ip6tables',

  }
  -> firewall { '002 v6 reject local traffic not on loopback interface':
    iniface     => '! lo',
    proto       => 'all',
    destination => '::1/128',
    action      => 'reject',
    provider    => 'ip6tables',

  }
  -> firewall { '003 v6 accept related established rules':
    proto    => 'all',
    state    => ['RELATED', 'ESTABLISHED'],
    action   => 'accept',
    provider => 'ip6tables',

  }
  -> firewall { '004 v6 Allow inbound SSH':
    dport    => 22,
    proto    => tcp,
    action   => accept,
    provider => 'ip6tables',

  }


}
