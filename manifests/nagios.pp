# installs nagios and smokeping
class put::nagios {

  # https://www.altsec.info/check_scan.html
  # https://lpc.events/event/4/contributions/436/attachments/217/388/cable-diagnostics.pdf
  # https://www.youtube.com/watch?v=sfz1tJIX-XA
  # https://forum.openwrt.org/t/tool-for-vct-virtual-cable-test-for-qca-switches-and-others-wanted/106629
  # https://www.youtube.com/watch?v=Z7IH14068Yg


    $pkgs = [
      'nagvis',
      'nagvis-demos',
      'nagios4',
      'smokeping'
    ]
    ensure_packages( $pkgs)

   service {'smokeping':
    ensure => 'running',
    enable => true,
    require => Package['smokeping'],
  }

   file { "/etc/smokeping/config.d/Targets":
    content => epp("${module_name}/smokeping_Targets.epp"),
    notify => Service['smokeping'],
  } 

   service {'nagios4':
    ensure => 'running',
    enable => true,
    require => Package['nagios4'],
  }

   file { "/etc/nagios4/conf.d/switch.cfg":
    content => epp("${module_name}/nagios_objects.epp"),
    notify => Service['nagios4'],
  } 

}
