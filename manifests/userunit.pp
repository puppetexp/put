#systemd user unit
define put::userunit (
  String $unit_name = $title,
  String $type = 'simple',
  String $user = 'user',
  #String $userid = '1000',
  String $command = '/usr/bin/sleep 60',
  String $display = hiera('put::defaultdisplay', ':0'),
  String $defaultdisplay = hiera('put::defaultdisplay', ':unset'),
  $after = [],
  $beforewtf = [ ],
  $environment = [],
  String $cwd = '/tmp/',
  Boolean $autorefresh = true,
  Boolean $autostart = false,
  Boolean $autorestart = false,
  $wantedby = [ ],
  $ensure = present,
  $wants = [],
  $partof = [],
  $execstartpre = [],
  $execstartpost = [],

  ){

#  $userid = $users[$user][userid]
  if has_key($::facts[users], $user) {
  #if $::facts[users][$user] {
    $userid = $::facts[users][$user][userid]
  } else {
    $userid = '1000'
  }
#  notify{ "userid: $userid": 
#$envextra = ["TESTJEFORDISPLAY=${display}", "DEFAULTDISPLAY=${defaultdisplay}" ,  "XDG_RUNTIME_DIR=/run/user/${userid}", 'XAUTHORITY=/home/user/.Xauthority' ]
  $envextra = []

  $allenv = $environment + $envextra

  $unit_template = @(END)
<%- | String $name, String $type ,String $cwd,  Array $after, Array $before, String $command, Boolean $autorestart, Boolean $autostart, $wantedby, Array $wants, Array $partof, Array $execstartpre, Array $execstartpost | -%>
[Unit]
Description=<%= $name %>
StartLimitIntervalSec=0
#StartLimitInterval=200
#StartLimitBurst=5
#PartOf=xsession.target
After=xsession_started.target
Wants=xsession_started.target
<% unless $partof =~ Array[Data,0,0] { -%>
<% $partof.each -%>
<% |$a| { %>PartOf=<%= $a %>
<% } -%>
<% } -%>


<% unless $after =~ Array[Data,0,0] { -%>
<% $after.each -%>
<% |$a| { %>After=<%= $a %>
<% } -%>
<% } -%>

<% unless $before =~ Array[Data,0,0] { -%>
<% $before.each -%>
<% |$a| { %>Before=<%= $a %>
<% } -%>
<% } -%>

<% unless $wants =~ Array[Data,0,0] { -%>
<% $wants.each -%>
<% |$a| { %>Wants=<%= $a %>
<% } -%>
<% } -%>


[Service]
Type=<%= $type %>
ExecStart=<%= $command %>

<% unless $execstartpre =~ Array[Data,0,0] { -%>
<% $execstartpre.each -%>
<% |$a| { %>ExecStartPre=<%= $a %>
<% } -%>
<% } -%>

<% unless $execstartpost =~ Array[Data,0,0] { -%>
<% $execstartpost.each -%>
<% |$a| { %>ExecStartPost=<%= $a %>
<% } -%>
<% } -%>




WorkingDirectory=<%= $cwd %>

EnvironmentFile=/etc/default/sms

<% unless $allenv =~ Array[Data,0,0] { -%>
<% $allenv.each -%>
<% |$e| { %>Environment=<%= $e %>

<% } -%>
<% } -%>


<% if $autorestart == true { -%>
Restart=always
#Restart=on-failure
RestartSec=30
<% } -%>

[Install]
<% if $autostart == true { -%>
WantedBy=default.target
<% } -%>

<% unless $wantedby =~ Array[Data,0,0] { -%>
<% $wantedby.each -%>
<% |$e| { %>WantedBy=<%= $e %>

<% } -%>
<% } -%>




END

  ### linger: sudo loginctl enable-linger "$USER"

  ### systemctl --user -t target

  ### systemctl daemon-reload

  include put::userunitprep

# FIXME: linger doesn ot work on debian
#  exec { "enable linger ${unit_name}":
#    command => "loginctl enable-linger ${user}",
#    path    => '/usr/local/bin:/usr/bin:/bin',
#    #user    => $user,
#    #environment => ["HOME=/home/${user}", 'XDG_RUNTIME_DIR=/run/user/1000' ]
#
#  }

  if $ensure == 'present' {
    file { "/home/${user}/.config/systemd/user/${name}.service":
      ensure  => file,
      content => inline_epp($unit_template, {
        'name'          => $unit_name,
        'type'          => $type,
        'command'       => $command,
        'after'         => $after,
        'before'         => $beforewtf,
        'cwd'           => $cwd,
        'autostart'     => $autostart,
        'autorestart'   => $autorestart,
        'wantedby'      => $wantedby,
        'wants'         => $wants,
        'partof'        => $partof,
        'execstartpre'  => $execstartpre,
        'execstartpost' => $execstartpost,

      }),
      notify  =>  [  Exec["daemon reload userservice ${unit_name}"],  Exec["enable userservice ${unit_name}"], Exec["start (allow failure) userservice ${unit_name}"],
                    Exec["restart (allow failure) userservice ${unit_name}"],
                  ]

    }
  } else {
    file { "/home/${user}/.config/systemd/user/${name}.service":
      ensure => absent,
      notify =>  [  Exec["daemon reload userservice ${unit_name}"] ],
    }
  }
  -> exec { "daemon reload userservice ${unit_name}":
      command     => 'systemctl --user daemon-reload ', 
      path        => '/usr/local/bin:/usr/bin:/bin',
      user        => $user,
      environment => ["HOME=/home/${user}", "XDG_RUNTIME_DIR=/run/user/${userid}" ],
      refreshonly => true,

  }
  -> exec { "enable userservice ${unit_name}":
      command     => "systemctl --user enable ${unit_name} ",
      path        => '/usr/local/bin:/usr/bin:/bin',
      user        => $user,
      environment => ["HOME=/home/${user}", "XDG_RUNTIME_DIR=/run/user/${userid}" ],
      refreshonly => true,

  }
  -> exec { "start (allow failure) userservice ${unit_name}":
      command     => "systemctl --user start ${unit_name} || true",
      path        => '/usr/local/bin:/usr/bin:/bin',
      user        => $user,
      environment => ["HOME=/home/${user}", "XDG_RUNTIME_DIR=/run/user/${userid}" ],
      refreshonly => true,

  }
  -> exec { "restart (allow failure) userservice ${unit_name}":
      command     => "systemctl --user restart ${unit_name} || true",
      path        => '/usr/local/bin:/usr/bin:/bin',
      user        => $user,
      environment => ["HOME=/home/${user}", "XDG_RUNTIME_DIR=/run/user/${userid}" ],
      refreshonly => true,

  }
#  exec { "status userservice ${unit_name}":
#      command     => "systemctl --user status ${unit_name}",
#      path        => '/usr/local/bin:/usr/bin:/bin',
#      user        => $user,
#      environment => ["HOME=/home/${user}", "XDG_RUNTIME_DIR=/run/user/${userid}" ],
#      #refreshonly => true,
#}




}


