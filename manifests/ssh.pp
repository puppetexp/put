# adds ssh keys from  hiera
class put::ssh {

$pkgs = [
	'openssh-server'
]

ensure_packages($pkgs, {ensure => 'installed'})



# ssh server
  file { "/etc/ssh/sshd_config":
    content => epp("${module_name}/sshd_config.epp"),
    #notify => Service['ssh'],
  }


  
  hiera_hash('admins').each |String $username, Hash $user_hash| {
    if $user_hash['sshkeys'] =~ Array {
      #notify {"sshkeys is array !": }

      $user_hash[sshkeys].each |Hash $sshkey| {
        #notify { "$username  + $sshkey['type'] ": } 

        # disabled, cause that user does not exist everywhere 
        #        ssh_authorized_key { "user: ${username}":
        #  ensure => present,
        #  user   => 'user',
        #  type   => $sshkey['type'],
        #  key    => $sshkey['key']
        #}

        ssh_authorized_key { "root: ${username}":
          ensure => present,
          user   => 'root',
          type   => $sshkey['type'],
          key    => $sshkey['key']
        }
      }

    }  else {
      fail ('sshkeys we are expected an array, check hiera !')
    }

  }

}
