# awesomewm desktop 
class put::awesomedesktop
(
  String $xfsettings = '',
  String $user = 'user',
  Boolean $softwaregl = false, 
)
{
  class {'put::commondesktop':
    softwaregl => $softwaregl,
    session => 'awesome',
  }


  $pkgs = [
    'awesome',
    'awesome-extra',
  ]

  ensure_packages($pkgs, {ensure => 'installed'})

  include put::scripts

  file { "/home/${user}/.config/awesome/":
          ensure  => directory,
          owner   => $user,
          group   => $user,
          mode    => '0755',
          recurse => true,
          replace => true,
          source  => "puppet:///modules/put/awesome",


  }
}
