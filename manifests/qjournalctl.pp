# qjournalctl
class put::qjournalctl
(
){

  $pkgs = [
    'libssh-dev',
  ]

  ensure_packages($pkgs, {ensure => 'installed'}) 


  put::buildfromgit { 'qjournalctl':
    revision => 'master',
    repo      => 'https://github.com/pentix/qjournalctl.git',
    commands  => [
      './autogen.sh',
      'make',
      'make install',
    ]
  } 



}
