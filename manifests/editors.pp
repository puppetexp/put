# configure editors
class put::editors {
  require put::nodejs
  $pkgs = [
    'vim',
    'emacs',
    'curl',
    'puppet-lint'
  ]

  ensure_packages($pkgs, {ensure => 'installed'})


  $vimrc = @(EOF)

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source /etc/vim/vimrc.local
\| endif

  syntax on
  set hlsearch
  set expandtab
  set shiftwidth=2
  set softtabstop=2
  set tabstop=2
  filetype plugin indent on
  autocmd FileType html setlocal expandtab shiftwidth=2 tabstop=2
  autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4
  autocmd FileType yaml setlocal expandtab tabstop=2 expandtab shiftwidth=2 softtabstop=2

call plug#begin('~/.config/nvim/plugged')

Plug 'editorconfig/editorconfig-vim' " For filetype management.
Plug 'elzr/vim-json' " For metadata.json
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " Install fuzzy finder. Use whatever you prefer for file browsing
Plug 'junegunn/fzf.vim' " Fuzzy Finder vim plugin
Plug 'mrk21/yaml-vim' " For hieradata
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}} " Language Server support
Plug 'rodjek/vim-puppet' " For Puppet syntax highlighting
Plug 'vim-ruby/vim-ruby' " For Facts, Ruby functions, and custom providers
Plug 'ocaml/vim-ocaml'
Plug 'prabirshrestha/vim-lsp'
call plug#end()

  EOF

  file { '/root/.vimrc':
    content => $vimrc,
  }

  file { '/etc/vim/vimrc.local':
    content => $vimrc,
  }


}
