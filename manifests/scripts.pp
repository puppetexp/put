# supposed to copy bunch of scripts to /usr/local/bin/
class put::scripts {

  file { '/usr/local/bin/xfsave.sh':
    source  => 'puppet:///modules/put/localbin/xfsave.sh',
    recurse => 'remote',
    mode    => '0755',
  }
  file { '/usr/local/bin/xfrestore.sh':
    source  => 'puppet:///modules/put/localbin/xfsave.sh',
    recurse => 'remote',
    mode    => '0755',
  }





}
