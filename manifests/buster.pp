# debian buster
class put::buster {
tag 'slow'

### FIXME: figure out how to do that (without having to use hiera)
#  apt { 'purge':
#    purge => {
#      "/etc/apt/sources.list" => true
#    }
#  }
#  ->
    apt::source { 'debian_stable':
    ensure   => absent,
    comment  => 'Debian stretch',
    location => 'http://httpredir.debian.org/debian/',
    release  => 'stretch',
    repos    => 'main contrib non-free',
    include  => {
      'src' => true,
      'deb' => true,
    },
  }

  apt::source { 'debian_stretch':
    ensure   => absent,
    comment  => 'Debian stretch',
    location => 'http://httpredir.debian.org/debian/',
    release  => 'stretch',
    repos    => 'main contrib non-free',
    include  => {
      'src' => true,
      'deb' => true,
    },
  }
  -> apt::source { 'stretch_backport':
    ensure   => absent,
    location => 'http://deb.debian.org/debian/',
    release  => 'stretch-backports',
    repos    => 'main',
    pin      => '900',
    include  => {
      'src' => true,
      'deb' => true,
    },
  }
  -> apt::source { 'debian_buster':
    comment  => 'Debian buster',
    location => 'http://httpredir.debian.org/debian/',
    release  => 'buster',
    repos    => 'main contrib non-free',
    #pin      => '2000',
    include  => {
      'src' => true,
      'deb' => true,
    },
  }
  -> apt::source { 'buster_backport':
    ensure   => present,
    location => 'http://deb.debian.org/debian/',
    release  => 'buster-backports',
    #pin      => '90',
    repos    => 'main',
    #FIXME: is the pin a good or a bad idea??
    #pin      => '90',
    include  => {
      'src' => true,
      'deb' => true,
    },
  }
#  -> exec { 'apt-update release change':
#    command  => '/usr/bin/apt-get update --allow-releaseinfo-change',
#    path     => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
#    schedule => 'everyday',
#
#  }
  exec { 'apt-update':
    command => '/usr/bin/apt-get update',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
#    schedule => 'everyday',

  }
  -> exec { '--configure -a':
    command   => '/usr/bin/dpkg --configure -a',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
#    schedule  => 'everyday',

  }

  -> exec { 'apt --fix-broken install':
    command   => '/usr/bin/apt --fix-broken install',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
#    schedule  => 'everyday',

  }
  -> exec { 'apt install':
    command   => '/usr/bin/apt --fix-broken install',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
#    schedule  => 'everyday',

  }

  -> exec { 'apt-upgrade':
    command   => '/usr/bin/apt-get --quiet --yes --allow-downgrades --fix-broken upgrade',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
#    schedule  => 'everyday',

  }

  -> exec { 'apt-get dist-upgrade':
    command   => '/usr/bin/apt-get --quiet --yes --fix-broken --allow-downgrades dist-upgrade',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
#    schedule  => 'everyday',

  }

  -> exec { 'apt-upgrade2':
    command   => '/usr/bin/apt-get --quiet --yes --allow-downgrades --fix-broken upgrade',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],
#    schedule  => 'everyday',

  }

}
