# signal
class put::signal {
  require put::debian


  exec { 'import signal gpg key (is not on keyserver)':
    command => 'wget -O- https://updates.signal.org/desktop/apt/keys.asc | apt-key add -',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',

  }


  #apt::key { 'Open whispersystems':
  #  id     => 'DBA36B5181D0C816F630E889D980A17457F6FB06',
  #  server => 'pgp.mit.edu',
 # } ->
    -> apt::source { 'signal_apt':
    ensure   => present,
    comment  => 'Debian stretch',
    location => 'https://updates.signal.org/desktop/apt',
    release  => 'xenial',
    repos    => 'main',
    include  => {
      'src' => false,
      'deb' => true,
    },
  }

  -> exec { 'apt-update for signal':
    command => '/usr/bin/apt-get update',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',

  }

  -> exec { 'apt install for signal':
    command   => '/usr/bin/apt install -y signal-desktop',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],

  }


}
