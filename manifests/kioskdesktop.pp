# kiosk desktop 
class put::kioskdesktop
(
  String $user = 'user',
  Boolean $softwaregl = false, 
)
{
  class {'put::commondesktop':
    softwaregl => $softwaregl,
    session => 'openbox',
  }

  $rmpkgs = [
    'xfce4',
    'xfce4-power-manager',
    'rpi-connect',
    'cups',
    'cups-daemon',
    'modemmanager',
    'networkmanager',
    'tor',

  ]

  ensure_packages($rmpkgs, {ensure => 'absent'})



  $pkgs = [
    'openbox',
    'imagemagick'
  ]

  ensure_packages($pkgs, {ensure => 'installed'})

  #include put::scripts

    $openbox_autostart = @(EOF)
xset -dpms
xset s off
xhost +
xrandr --output HDMI-0 --mode 1920x1200
#chromium  --noerrdialogs --kiosk --incognito  http://google.com
display -resize 1920x1200 -background "#616161" -borderwidth 1 -bordercolor orange -foreground white -backdrop /home/user/src/More-Heat-Than-Light/logo.jpg
EOF

  file { '/etc/xdg/openbox/autostart':
    content => $openbox_autostart,
    mode    => '0755',
  }



}
