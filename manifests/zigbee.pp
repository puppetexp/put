# configure editors
class put::zigbee {

  require put::nodejs
  require mosquitto
  
  $pkgs = [
    'curl',
    'wget'
  ]

  ensure_packages($pkgs, {ensure => 'installed'})


  docker::run { 'zigbee2mqtt':
    image   => 'koenkk/zigbee2mqtt',
    env     => [  
                'TZ=Europe/Amsterdam',
               ],
    devices => [
      '/dev/serial/by-id/usb-ITead_Sonoff_Zigbee_3.0_USB_Dongle_Plus_264f0e2472e7ed11a1dd6d6262c613ac-if00-port0:/dev/ttyACM',
	],
    volumes => [ 
      '/opt/zigbee/data',
      '/run/udev:/run/udev',
    ],
    ports   => [
      '5250:5250',
   ],
  }


}
