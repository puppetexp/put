# defined type to build software from git
#
# @example Declaring the class
#   lalalal
#
# @repo git url
# @revision git revision
# @commands  can be a array of strings or a array of hashes
# @srcdir where to clone the repo
# @cwd default path for commands, will be created (for ex. build for cmake etc.), defaults to repo root
# @creates file that will be  created ex. /usr/local/bin/[program]
# @user user that should own the files and exec the commands
define put::buildfromgit (
#  $repo_name = $title,
  $repo_name = $name,
  String $repo = 'git://git.ardour.org/ardour/ardour.git',
  Optional[String] $revision = 'master',
  #commands can be a array of strings or a array of hashes
  $commands = ['bash waf configure', 'bash waf'],
  #$commandshash = [
  #  { cmd => 'bash waf configure', cwd => 'lala' },
  #  { cmd => 'lalalala', cwd => 'adsf' }
  #],
  String $srcdir = '/usr/local/src',
  #default path for commands, will be created (for ex. build for cmake etc.)
  Optional[String] $cwd = undef,
  Optional[String] $creates = undef,
  Optional[Array] $python3deps = undef,
  Optional[Array] $debiandeps = undef,
  Optional[Array] $debianbuilddeps = undef,
  Optional[Array] $python2deps = undef,
  Optional[Array] $nodedeps = undef,
  Array $env = [],
  String $user = 'root',
  ){



  $repo_dir = "${srcdir}/${repo_name}"

  # When to rebuild:
  #   * when upstream git has changes
  #   * when previous build failed (any of the commands failed, or creates file not created)

  #$shouldwebuild = 

#  file { "/tmp/${repo_name}_facts.yaml":
#    #content => inline_template(' scope.to_hash.reject { |k,v| !( k.is_a?(String) && v.is_a?(String) ) }.to_yaml '),
#    content  => inline_template("<%= scope.to_hash.reject { |k,v| k.to_s =~ /(lalala|free)/ }.to_yaml %>"),
#  }

  vcsrepo { "${srcdir}/${repo_name}":
    ensure     => latest,
    provider   => git,
    revision   => $revision,
    source     => $repo,
    submodules => true,
    owner      => $user,

  }


  #create build directory or somethign
  if $cwd != undef {

    $wd = "${repo_dir}/${cwd}"

    file { "${repo_dir}/${cwd}":
      ensure => directory,
      owner  => $user,
    }
  } else {
    $wd = $repo_dir
  }

  Exec {
    user    => $user,
    cwd     => $wd,
    timeout => 9999,
    path    => "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:${srcdir}:${wd}:${repo_dir}",
    environment => ['HOME=/root', 'LC_CTYPE=en_US.UTF-8',  ] + $env,
  }

  if $debianbuilddeps != undef {
    each($debianbuilddeps) | $pkg| {
      exec { "debianbuilddep: ${pkg}":
        command => "apt-get -y build-dep ${pkg}",
        user    => 'root',
      }
    }
  }

  if $python3deps != undef {
    each($python3deps) | $pkg| {
      package { "python3-${pkg}":
        ensure   => installed,
        name     => $pkg,
        provider => 'pip3',
      }
    }
  }

  if $python2deps != undef {
    each($python2deps) | $pkg| {
      package { "python2-${pkg}":
        ensure   => installed,
        name     => $pkg,
        provider => 'pip',
      }
    }
  }

  if $debiandeps != undef {
    ensure_packages( $debiandeps,
    {
      ensure   => installed,
    })
  }
  if $nodedeps != undef {
    ensure_packages( $nodepkgs,
    {
      ensure   => installed,
      provider => 'npm',
      install_options => [ '-g' ],
    })
  }

# FIXME: actually make this whole thing that runs rebuild cmd's on changes actually work
#write a file with puppet run id
#  exec {"${repo_name} echo ${transaction_uuid} > ${srcdir}/current_puppet_run":
#    command => "echo ${transaction_uuid} > ${srcdir}/current_puppet_run",
#    user    => 'root',
#  }

#write a file with puppet run id when there is changes in the repo
#  exec {"${srcdir}/${repo_name}.haschanged":
#    subscribe   => Vcsrepo["${srcdir}/${repo_name}"],
#    refreshonly => true,
#    command     => "echo ${transaction_uuid} > ${srcdir}/${repo_name}.haschanged",
#    user        => 'root',
#  }

#  $prevcmd = "${srcdir}/${repo_name}.haschanged"
#  exec {"${name}: shouldrebuild":
#      command => 'echo rebuild',
#      #subscribe   => Vcsrepo["${srcdir}/${repo_name}"],
#      #refreshonly => true,
#      #onlyif "cat ${srcdir}/{$repo_name}.haschanged -eq ${repo_name} OR ${creates} does not exist} "
#      onlyif  => "cmp ${srcdir}/current_puppet_run} ${srcdir}/${repo_name}.haschanged",
#      user    => 'root',
#    }

  exec {"${srcdir}/${repo_name}.lala123":
    subscribe   => Vcsrepo["${srcdir}/${repo_name}"],
    refreshonly => true,
    command     => "date > ${srcdir}/${repo_name}.lala123",
    user        => 'root',
  }

  $prevcmd = "${srcdir}/${repo_name}.lala123"

  $cmddefaults = {
    cmd => '/bin/false',
    cwd => $wd,
    something => 'lala'
    }

  each($commands) | $command| {

    if $command =~ String {
      $cmdhash = $cmddefaults + { cmd => $command }
    }
    elsif $command =~ Hash {
      $cmdhash = $cmddefaults + $cmd
    }
    else {
      fail("${command} is not a string or a hash")
    }
    #depend on previous
    #->  
    exec {"${name}: ${command}":
      command => $cmdhash[cmd],
      cwd     => $cmdhash[cwd],
      user    => $user,
      #subscribe   => Vcsrepo["${srcdir}/${repo_name}"],
      #refreshonly => true,
      #onlyif "cat ${srcdir}/{$repo_name}.haschanged -eq ${repo_name} OR ${creates} does not exist} ",
      #onlyif => "cmp ${srcdir}/current_puppet_run} ${srcdir}/${repo_name}.haschanged",
      # FIXME: how to do this??
      require => Exec[$prevcmd],
      timeout => 10800,

    }
    #FIXME: how to do this??
    $prevcmd = "${name}: ${command}"

  }

#  exec {"${name}: buildsuccess":
#    command => "echo ${transaction_uuid} > ${srcdir}/${repo_name}.success",
#    #subscribe   => Vcsrepo["${srcdir}/${repo_name}"],
#    #refreshonly => true,
#    #onlyif "cat ${srcdir}/{$repo_name}.haschanged -eq ${repo_name} OR ${creates} does not exist} "
#    onlyif  => "cmp ${srcdir}/current_puppet_run} ${srcdir}/${repo_name}.haschanged",
#    user    => 'root',
#  }
}


