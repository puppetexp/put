# installs nodejs
class put::nodejs {


  case $facts['os']['distro']['codename']  {
    'buster': {
        class { 'nodejs':
          #repo_url_suffix => '14.x',
          nodejs_dev_package_ensure => 'present',
#          npm_package_ensure        => 'present',
 
        }
    }
    'bullseye': {
        class { '::nodejs':
          manage_package_repo       => false,
          nodejs_dev_package_ensure => 'present',
          npm_package_ensure        => 'present',
        }
    }
    'bookworm': {
      $pkgs = [
        'libnode-dev',
        'npm',
        'nodejs'
      ]
      ensure_packages($pkgs, {ensure => installed})
    }
    'trixie': {
      $pkgs = [
        'libnode-dev',
        'npm',
        'nodejs'
      ]
      ensure_packages($pkgs, {ensure => installed})
    }
 
    default: {
      fail("Unsupported Debian-flavor machine: ${facts['os']['distro']['codename'] }")
    }
  }





  $nodepkgs = [
    'yarn',
  ]
  ensure_packages( $nodepkgs,
  {
    ensure   => latest,
    provider => 'npm',
    install_options => [ '-g' ],
  })

}
