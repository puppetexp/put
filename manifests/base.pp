# base profile, should work for all servers and desktop
class put::base {

  include git


  schedule { 'everyday':
    period => daily,
    #  range  => '2 - 4',
  }


  exec { 'tasksel':
    command => '/usr/bin/tasksel install standard',
    creates => '/usr/sbin/useradd',
  }

  #include apt

# ssh client config
  file { "/etc/ssh/ssh_config":
    content => epp("${module_name}/ssh_config.epp"),
  }


  # set utf8 some modules fail otherwise
  if "${facts['os']['distro']['name']}" == "Debian" {

    require put::debian

    # ssh key from git.puscii.nl
sshkey { '185.52.224.4rsa':
  ensure       => present,
  type         => 'rsa',
  host_aliases => ['185.52.224.4'],
  key          => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDUrpYZnHBrJZ4W0qjCVVE+2RG1GE6dyu+WCtOQaySqXKHIQ68eiDkFjQhB6wEQ3nO8U94SuxfvU75o5+ZsSF4bWEg6tDcz4oK8885Qg6jvrfScyqT/zvr9UupTvKmFPAHUpSwTzoo3Cvd8v46VuyaRzimxfFYiUyFD6QtiejOWLWO4qNck+LZngXREML244lE8hzuYKhVHjLFynRpvo4/1dcuwd1Kaz/GgA5POUi5S5tSf/8Gq2wxWSIUODzLfSvYyj6obBouENADPjpv1JUahpdrLUJInIjxOkrcXShmd3v8tRY6o5O3KAu1DGdHUYL8QXsp2nJW2sn0z84ZBLivz',
}
sshkey { '185.52.224.4ed25519':
  ensure       => present,
  type         => 'ed25519',
  host_aliases => ['185.52.224.4'],
  key          => 'AAAAC3NzaC1lZDI1NTE5AAAAIN4fwf+wRaUgeu7DRjW2A/QAnSEdZdbUAWR1EfIQ2SIm',
}


  class { 'locales':
    default_locale => 'en_US.UTF-8',
    locales        => ['en_US.UTF-8 UTF-8'],
    lc_time        => 'en_DK.UTF-8',
    lc_paper       => 'de_DE.UTF-8',
  }


    $profile = @(EOF)
# /etc/profile: system-wide .profile file for the Bourne shell (sh(1))
# and Bourne compatible shells (bash(1), ksh(1), ash(1), ...).

if [ "`id -u`" -eq 0 ]; then
  PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
else
  PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games"
fi
export PATH

if [ "${PS1-}" ]; then
  if [ "${BASH-}" ] && [ "$BASH" != "/bin/sh" ]; then
    # The file bash.bashrc already sets the default PS1.
    # PS1='\h:\w\$ '
    if [ -f /etc/bash.bashrc ]; then
      . /etc/bash.bashrc
    fi
  else
    if [ "`id -u`" -eq 0 ]; then
      PS1='# '
    else
      PS1='$ '
    fi
  fi
fi

if [ -d /etc/profile.d ]; then
  for i in /etc/profile.d/*.sh; do
    if [ -r $i ]; then
      . $i
    fi
  done
  unset i
fi

printf '\e]2;%s\a' ":`hostname`";
EOF

file { '/etc/profile':
  content => $profile,
}


$pkgs = [
  'psmisc',
  'lsb-release',
  'net-tools',
  'augeas-tools',
  'strace',
  'screen',
  'htop',
  'man',
  'less',
  'build-essential',
  'less',
  'dos2unix',
  'ruby',
  'vim',
  #'ntp' conflicts with ptp.pp
]



ensure_packages($pkgs, {ensure => 'installed'})


include unattended_upgrades

alternatives { 'editor':
  path => '/usr/bin/vim.basic',
  }  
} else {
  $pkgs = [
    'vim',
    'screen',
  ]

  ensure_packages($pkgs, {ensure => 'installed'})

}



$screenrc = @(EOF)
caption string "%?%F%{= Bk}%? %C%A %D %d-%m-%Y %{= kB} %t%= %?%F%{= Bk}%:%{= wk}%? %n "
hardstatus alwayslastline
hardstatus string '%{= kG}[ %{G}%H %{g}][%= %{= kw}%?%-Lw%?%{r}(%{W}%n*%f%t%?(%u)%?%{r})%{w}%?%+Lw%?%?%= %{g}][%{B} %d/%m %{W}%c %{g}]'
EOF

file { '/root/.screenrc':
  content => $screenrc,
}



class { 'timezone':
  timezone => 'Europe/Amsterdam',
}



}
