class put::virtualbox {

apt::source { 'virtualbox':
	architecture => $facts['os']['architecture'],
	release => 'bookworm',
	location     => 'https://download.virtualbox.org/virtualbox/debian',
	repos        => 'contrib',
	key          => {
		'id'     => 'B9F8D658297AF3EFC18D5CDFA2F683C52980AECF',
		'source' => 'https://www.virtualbox.org/download/oracle_vbox_2016.asc',
	},
}

package { 'virtualbox-7.0':
  ensure => 'latest',
}

exec { '/usr/bin/vagrant plugin install vagrant-puppet-install':
  creates => '/root/.vagrant.d/gems/3.1.2/gems/vagrant-puppet-install-7.0.0/lib/vagrant-puppet-install/plugin.rb'
}

exec { '/usr/bin/vagrant plugin install vagrant-vbguest':
  creates => '/root/.vagrant.d/gems/3.1.2/gems/vagrant-vbguest-0.32.0/Readme.md'
}


Apt::Source['virtualbox'] -> Class['apt::update'] -> Package['virtualbox-7.0']

} 
