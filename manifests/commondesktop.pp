# common desktop stuff
class put::commondesktop
(
  String $user = 'user',
  Boolean $softwaregl = false,
  String $userdir = '/home/user',
  String $session = 'xfce4'
)
{
 require put::userunitprep
  require put::debian
  require put::base
  require put::extrabase
  require put::scripts
  require put::smsprep
  include put::scripts

  notice("commondesktop included")

  $pkgs = [
    'lightdm',
    'xserver-xorg-input-all',
    'xserver-xorg-input-evdev',
    'xserver-xorg-input-libinput',
    'i3lock',
  ]

  ensure_packages($pkgs, {ensure => 'installed'})

  $lockservice = @(EOF)
  [Unit]
  Description=i3lock
  Before=sleep.target

  [Service]
  User=user
  Type=forking
  Environment=DISPLAY=:0
  ExecStart=/usr/bin/i3lock

  [Install]
  WantedBy=sleep.target
  EOF

  file { '/etc/systemd/system/i3lock.service':
    content => $lockservice,
    mode    => '0755',
  }->
  exec { 'enable lockservcie':
    command => '/usr/bin/systemctl enable i3lock.service',
  }


  $xreset = @(EOF)
su -l user -c 'systemctl --user stop xsession.target'
EOF

  file { '/etc/X11/Xreset.d/99stop-xsession-target':
    content => $xreset,
    mode    => '0755',
  }

  $start_xsession_target = @(EOF)
set -o allexport
. /etc/default/sms
systemctl --user import-environment PATH DBUS_SESSION_BUS_ADDRESS DISPLAY XAUTHORITY
systemctl --no-block --user start xsession.target
xset s off         # don't activate screensaver
xset -dpms         # disable DPMS (Energy Star) features.
xset s noblank     # don't blank the video device
sleep 1
systemctl --no-block --user start xsession_started.target
EOF

  file { '/etc/X11/Xsession.d/999start-xsession-target':
    content => $start_xsession_target,
    mode    => '0755',
  }


  $xsession_unit = @(EOF)
[Unit]
Description=Xsession running
#FIXME: was this there for a reason????
#After=xfce4-notifyd.service
#Requires=xfce4-notifyd.service

#BindsTo=graphical-session.target
EOF




$screenrc = @(EOF)
caption string "%?%F%{= Bk}%? %C%A %D %d-%m-%Y %{= kB} %t%= %?%F%{= Bk}%:%{= wk}%? %n "
hardstatus alwayslastline
hardstatus string '%{= kG}[ %{G}%H %{g}][%= %{= kw}%?%-Lw%?%{r}(%{W}%n*%f%t%?(%u)%?%{r})%{w}%?%+Lw%?%?%= %{g}][%{B} %d/%m %{W}%c %{g}]'
EOF

file { "/home/${user}/.screenrc":
  content => $screenrc,
}


  file { "/home/${user}/.config/systemd/user/xsession.target":
    content => $xsession_unit,
  }

  $xsession_started_unit = @(EOF)
[Unit]
Description=Xsession started
EOF

  file { "/home/${user}/.config/systemd/user/xsession_started.target":
    content => $xsession_started_unit,
  }


  $softwareglxsession = @(EOF)
export LIBGL_ALWAYS_SOFTWARE=1

EOF

if $softwaregl == true {
# FIXME: do this on computers without proper videocard
  file { '/etc/X11/Xsession.d/10software-opengl':
    content => $softwareglxsession,
    mode    => '0755',
  }
}

  $ldmconf = @("EOF")
[LightDM]
[Seat:*]
autologin-user-timeout=5
autologin-user=user
autologin-session=${session}
session-cleanup-script=/etc/X11/Xreset.d/99stop-xsession-target


[XDMCPServer]
#[VNCServer]
#enabled=true
#command=Xvnc
#port=5900
#listen-address=
#width=1024
#height=768
#depth=8
EOF

  file { '/etc/lightdm/lightdm.conf':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0755',
    content => $ldmconf,
  }->
  exec { 'start lightdm':
    command => '/usr/bin/systemctl start lightdm',
  }

  service {  'lightdm':
    ensure =>  running
  }

}
