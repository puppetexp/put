# base profile, should work for all servers and desktop
class put::extrabase {

  include git

  require put::debian

  # that makes the user
  require put::userunitprep


  $responsefile = @("EOF")
firmware-ipw2x00	firmware-ipw2x00/license/accepted	boolean	true
firmware-ipw2x00	firmware-ipw2x00/license/error	error	
EOF

  file { '/tmp/licence_responsefile':
    content => $responsefile,
  }

  case $facts['os']['distro']['codename']  {
  'buster': {
    $pkgs = [
        'debian-goodies',
        #'apparmor',
        #'apparmor-easyprof',
        #'apparmor-profiles',
        #'apparmor-utils',
        #'apparmor-profiles-extra',
        'initramfs-tools',
        'amd64-microcode',
        'firmware-amd-graphics',
        'firmware-atheros',
        'firmware-bnx2',
        'firmware-bnx2x',
        'firmware-brcm80211',
        'firmware-cavium',
        'firmware-intel-sound',
        # does not exist'firmware-intelwimax',
        #FIXME: stupid licence stuff # 
        'firmware-ipw2x00',
        'firmware-iwlwifi',
        'firmware-linux',
        'firmware-linux-nonfree',
        'firmware-misc-nonfree',
        #'firmware-qcom-media',
        'firmware-qlogic',
        'firmware-ralink',
        'firmware-realtek',
        'firmware-samsung',
        'firmware-siano',
        'firmware-ti-connectivity',
        'intel-microcode',
        'memtest86+',
      ]
   }

   'bullseye': {
     $pkgs = [
        'debian-goodies',
        #'apparmor',
        #'apparmor-easyprof',
        #'apparmor-profiles',
        #'apparmor-utils',
        #'apparmor-profiles-extra',
        'initramfs-tools',
        'amd64-microcode',
        'firmware-amd-graphics',
        'firmware-atheros',
        'firmware-bnx2',
        'firmware-bnx2x',
        'firmware-brcm80211',
        'firmware-cavium',
        'firmware-intel-sound',
        # does not exist'firmware-intelwimax',
        #FIXME: stupid licence stuff # 
        'firmware-ipw2x00',
        'firmware-iwlwifi',
        'firmware-linux',
        'firmware-linux-nonfree',
        'firmware-misc-nonfree',
        #'firmware-qcom-media',
        'firmware-qlogic',
        'firmware-ralink',
        'firmware-realtek',
        'firmware-samsung',
        'firmware-siano',
        'firmware-ti-connectivity',
        'intel-microcode',
        'memtest86+',
      ]


   }


  'bookworm': {

      $pkgs = [
        'initramfs-tools',
        'memtest86+',
        'firmware-amd-graphics',
         'firmware-linux-free',
        'firmware-linux-nonfree',
        'firmware-misc-nonfree',
 
      ]
  }
    'trixie': {

      $pkgs = [
        'initramfs-tools',
        'memtest86+',
        'firmware-amd-graphics',
        'firmware-linux-free',
        'firmware-linux-nonfree',
        'firmware-misc-nonfree',
      ]

    }


    default: {
      fail('unknown release')
    }
  }


  #ensure_packages($pkgs, {ensure => 'installed', responsefile => '/tmp/licence_responsefile'})


  class hosts ($hosts = hiera_hash("hosts"))  {
    create_resources( 'host', $hosts )
  }

}
