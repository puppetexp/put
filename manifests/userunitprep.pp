class put::userunitprep (
  Boolean $linger = false,
  Boolean $setpw = false,
  )
{

#FIXME: make this work for other users
$user = 'user';

$pkgs = [
  'bluetooth',
  ]

  ensure_packages($pkgs, {ensure => 'installed'})
  group { 'user':
    ensure =>  present
  }

  if $setpw {
  
    user { 'user':
      ensure     => present,
      comment    => 'the user',
      home       => '/home/user',
      managehome => true,
      shell      => '/bin/bash',
      groups     => ['cdrom', 'floppy', 'audio', 'dip', 'video', 'plugdev', 'users', 'netdev', 'bluetooth', 'user'],
      password  => pw_hash('user', 'SHA-512', 'salty')
    }

  } else {

    user { 'user':
      ensure     => present,
      comment    => 'the user',
      home       => '/home/user',
      managehome => true,
      shell      => '/bin/bash',
      groups     => ['cdrom', 'floppy', 'audio', 'dip', 'video', 'plugdev', 'users', 'netdev', 'bluetooth', 'user'],
      #password  => pw_hash('user', 'SHA-512', 'salty')
    }

 

  }->
  #FIXME: otherwise no dbus when user is not logged in???

  if $linger {
	  exec { 'enable lingering':
	    command => '/usr/bin/loginctl enable-linger user',
	  }
  } else {
	  exec { 'disable lingering':
	    command => '/usr/bin/loginctl disable-linger user',
	  }


  }


  file { "/home/${user}/.config":
    ensure => directory,
    owner  => $user
  }
  -> file { "/home/${user}/.config/systemd":
    ensure => directory,
    owner  => $user
  }
  -> file { "/home/${user}/.config/systemd/user":
    ensure => directory,
    owner  => $user
  }
-> file { "/home/${user}/.config/xfce4":
    ensure => directory,
    owner  => $user
  }
-> file { "/home/${user}/.config/xfce4/xfconf":
    ensure => directory,
    owner  => $user
  }
-> file { "/home/${user}/.config/xfce4/xfconf/xfce-perchannel-xml":
    ensure => directory,
    owner  => $user
  }





  file { '/var/lib/systemd/linger/':
    ensure => directory,
    #owner  => 'user',
  }
  file { "/var/lib/systemd/linger/${user}":
    ensure => present,
    #owner  => 'user',
  }



}
