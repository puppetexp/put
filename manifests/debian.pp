# debian buster
class put::debian {

  $pkgs = [
    'lsb-release',
  ]
  ensure_packages($pkgs, {ensure => 'installed'})



  case $facts['os']['distro']['codename'] {
    'buster': {
      require put::buster
    }
    'bullseye': {
      require put::bullseye
    }
    'bookworm': {
      require put::bookworm
    }
    'trixie': {
      require put::trixie
    }


    default: {
      fail("Unsupported Debian-flavor machine: ${ $facts['os']['distro']['codename'] }")
    }
  }

}
