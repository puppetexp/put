
small puppet utilities that are too small to get their own module:

userunit.pp, userunitprep.pp: creates systemd user units 

debian.pp, buster.pp, stretch.pp: sets up debian package repos

signal.pp: install signal messenger

buildfromgit.pp: builds software by cloning a git repo and running some commands

editors.pp: add your favorite editor config here

firewall_post.pp, firewall_pre.pp, firewall.pp: firewall config

scripts.pp: contains some scripts, probably should not exist

sshhs.pp: sets up ssh over hidden service

base.pp, extrabase.pp : basic config for every system

nodejs.pp: installs npm / nodejs

xfdesktop.pp: xfce desktop + user with autologin





