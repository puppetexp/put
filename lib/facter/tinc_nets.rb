require 'digest'
require 'base64'
require 'socket'

# TODO: read name from tinc conf
hostname = Socket.gethostname

Facter.add(:tinc_nets) do
  setcode do
    nets_array = Array.new()
    if File.exist?('/etc/tinc') 
      Dir.foreach('/etc/tinc') do |item|
        next if item == '.' or item == '..' or item == 'nets.boot'
        net = {} 
        net['netname'] = item
        tinc_conf = '/etc/tinc/' + item + '/tinc.conf'
        host_file = '/etc/tinc/' + item + '/hosts/' + hostname
        pub_key = '/etc/tinc/' + item + '/rsa_key.pub'

       
        if File.exist?(tinc_conf) 

          net['config'] = File.read(tinc_conf) 
          File.open(tinc_conf).each do |line|
            nw = line.gsub(/\s+/, "")
            ssp = nw.split('=')
            if ssp[0] and ssp[1]
              unless ssp[0].empty? || ssp[1].empty?
                net['conf_' + ssp[0]] = ssp[1]
              end
            end
            if line =~ /^Name$/
              net['conf_nnname'] = line 
            end
          end
        end

        if File.exist?(host_file)
          net['keyfile'] = File.read(host_file) 
          keyread = false
          key = ''
          File.open(host_file).each do |line|
            if keyread == true
              if line.include? "END RSA PUBLIC KEY-"
                keyread = false
              else
                key = key + line
              end
            else
              if line.include? "BEGIN RSA PUBLIC KEY"
                keyread = true
                key = ''
              elsif line.include? "="
                   ssphost = line.gsub(/\s+/, "").split('=')
                   if ssphost[0] and ssphost[1]
                      unless ssphost[0].empty? || ssphost[1].empty?

                         net['host_' + ssphost[0]] = ssphost[1]
                      end
                   end
              end 
            end 
          end
          net['key'] = key
        elsif File.exist?(pub_key)
          net['keyfile'] = File.read(pub_key) 
        end

        net['hostname'] = hostname



        nets_array.push(net)
      end
    end

    nets_array
  end
end




