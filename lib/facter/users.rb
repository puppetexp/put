require 'digest'
require 'base64'
require 'socket'

# TODO: read name from tinc conf
hostname = Socket.gethostname

Facter.add(:users) do
  setcode do
    users_hash = {}
    users_array = Array.new()
    if Facter.value(:kernel) == 'Linux'
        if File.exists?('/usr/bin/getent')
          # We work-around an issue in Facter #10278 by forcing locale settings ...
          ENV['LC_ALL'] = 'C'

          #
          # We utilise rely on "cat" for reading values from entries under "/proc".
          # This is due to some problems with IO#read in Ruby and reading content of
          # the "proc" file system that was reported more than once in the past ...
          #
          Facter::Util::Resolution.exec('/usr/bin/getent passwd').each_line do |line|
            # Remove bloat ...
            line.strip!

            # Turn line into a set of tokens ...
            userline = line.split(':')

            # Add user to list only if the user is not an essential system user ...
            user = {} 
            user['username'] = userline[0]
            user['userid'] = userline[2]
            users_hash[userline[0]] = user
            users_array.push(user) 
          end

        end
    end
    users_hash
  end
end
