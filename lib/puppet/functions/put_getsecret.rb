require 'json'
require 'securerandom'


Puppet::Functions.create_function(:put_getsecret) do
  dispatch :getsecret do
    param 'String', :path
    optional_param 'String', :secret_file
  end

  def init()
    if @secret_file.nil?
      Puppet.debug 'No secret file was set on function, defaulting to /etc/puppet/put_secrets.json'
      @secret_file = '/etc/puppet/put_secrets.json'
      raise Puppet::Error, 'No secret file' if @secret_file.nil?
    end
    
    if(File.exist?(@secret_file))
      ## lock the file
      begin
        @pws = JSON.parse(File.read(@secret_file), :symbolize_names => true)
      rescue JSON::ParserError
        # Handle error 
        raise Puppet::Error, "parsing secret_file failed"
      end
    else
      Puppet.debug "file does not exist, creating"

      emptyPws = {
        "passwordfile" => "true"
      }
      @pws = emptyPws
      File.open(@secret_file,"w") do |f|
        f.write(emptyPws.to_json)
      end
      File.chmod(0700,@secret_file)


    end

  end

  def getsecret(path, secret_file = nil)
    @secret_file = secret_file
    @path = path
    init()
    #puts @pws[path.to_sym]
    if @pws.key?(path.to_sym) 
      Puppet.debug "key found"
    else
      Puppet.debug "key not found, creating"
      @pws[path.to_sym] = SecureRandom.hex

    end
    #puts @pws
    writeout()
    Puppet::Pops::Types::PSensitiveType::Sensitive.new(@pws[path.to_sym])

  end


  def writeout() 
    File.open(@secret_file,"w") do |f|
      f.write(@pws.to_json)
    end
  end

#  @secret_file = 'testfile.json'
#  getsecret('mysql/testje', 'testfile.json')
#  getsecret('mysql/root', 'testfile.json')

end
